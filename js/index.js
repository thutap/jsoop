var arr = [];

var inputElement = document.getElementById("txt-nhapN");

inputElement.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    if (document.querySelector("#txt-nhapN").value.trim() == "") {
      return;
    }
    var number = document.querySelector("#txt-nhapN").value * 1;
    arr.push(number);
    document.getElementById("txt-number").innerHTML = arr;
  }
});

function nhapmang() {
  if (document.querySelector("#txt-nhapN").value.trim() == "") {
    return;
  }
  var number = document.querySelector("#txt-nhapN").value * 1;
  arr.push(number);
  document.getElementById("txt-number").innerHTML = arr;
}
function xoamang() {
  arr = [];
  document.getElementById("txt-number").innerHTML = "";
}

// bài 1
// số nguyên tố
function bai1() {
  function checknt(n) {
    var snt = true;

    if (n < 2) {
      return (snt = false);
    }

    for (var i = 2; i < n; i++) {
      if (n % i == 0) {
        snt = false;
        break;
      }
    }
    return snt;
  }
  var numbersnt = [];
  for (var i = 0; i < arr.length; i++) {
    if (checknt(arr[i]) == true) {
      numbersnt.push(arr[i]);
    }
  }
  if (numbersnt.length > 0) {
    document.getElementById(
      "txt-bai1"
    ).innerHTML = `Số Nguyên Tố có trong mảng là: ${numbersnt.join(", ")}`;
  } else {
    document.getElementById("txt-bai1").innerHTML = `Không có số nguyên tố: -1`;
  }
}

// bai 2
//  tính tổng các số dương có trong mảng
function bai2() {
  function tinhtong(arr) {
    var tong = 0;
    for (var i = 0; i < arr.length; i++) {
      tong += arr[i];
    }
    return tong;
  }
  document.getElementById(
    "txt-bai2"
  ).innerHTML = `Tổng các số dương có trong mảng là: ${tinhtong(arr)}`;
}

// bai 3
// Đếm số dương có trong mảng
function bai3() {
  function demso(arr) {
    var dem = 0;
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] > 0) {
        dem++;
      }
    }
    return dem;
  }
  document.getElementById(
    "txt-bai3"
  ).innerHTML = `Số dương có trong mảng là: ${demso(arr)}`;
}
// bai 4

//  số nhỏ nhất và lớn nhất có trong mảng
function bai4() {
  function minnumber(arr) {
    var min = arr[0];
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] < min) min = arr[i];
    }
    return min;
  }

  function maxnumber(arr) {
    var max = arr[0];
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] > max) max = arr[i];
    }
    return max;
  }
 
  document.getElementById(
    "txt-bai4"
  ).innerHTML = `Số Nhỏ nhất có trong mảng là: ${minnumber(
    arr
  )} </br> Số lớn nhất có trong mảng là: ${maxnumber(arr)}`;
}

